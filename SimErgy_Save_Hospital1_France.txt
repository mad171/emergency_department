R�capitulatif du fonctionnement de l'ED nomm� Hospital1 situ� en France
 


-------- Hospital1 in France  TIME = day:0  hour:1  min:49 --------

STAFF :
		1 Nurses -->		1 Idle,	0 Transporting,	0 OffDuty
		2 Physicians -->	2 Idle,	0 Visiting,	0 OffDuty
		1 Transporters -->	1 Idle,	0 Transporting,	0 OffDuty

FACILITIES :
		10 Stretchers -->	10 Free,	0 Occupied

ROOMS :
		1 WaitingRooms -->	1 Available,	0 Full
		1 BoxRooms -->		1 Free,		0 Occupied
		1 ShockRooms -->	1 Free,		0 Occupied

TESTROOMS :
		1 BloodRooms -->	1 Free,		0 Occupied
		1 MriRooms -->		1 Free,		0 Occupied
		1 RadioRooms -->	1 Free,		0 Occupied

PATIENTS L1   (Total : 1)
	0 Arriving,		0 Registered,		0 Transporting,			0 WaitingForConsultation,
	0 inConsultation,	0 WaitingForMRI,	0 WaitingForBloodTest,		0 WaitingForRadioTest
	0 Transporting,		0 WaitingForMRIStart,	0 WaitingForBloodTestStart,	0 WaitingForRadioStart
	0 Testing,		0 BloodTested,		0 MRITested,			0 RadioTested
	1 Released

PATIENTS L2   (Total : 2)
	0 Arriving,		0 Registered,		0 Transporting,			0 WaitingForConsultation,
	0 inConsultation,	0 WaitingForMRI,	0 WaitingForBloodTest,		0 WaitingForRadioTest
	0 Transporting,		0 WaitingForMRIStart,	0 WaitingForBloodTestStart,	0 WaitingForRadioStart
	0 Testing,		0 BloodTested,		0 MRITested,			0 RadioTested
	2 Released

PATIENTS L5   (Total : 3)
	0 Arriving,		0 Registered,		0 Transporting,			0 WaitingForConsultation,
	0 inConsultation,	0 WaitingForMRI,	0 WaitingForBloodTest,		0 WaitingForRadioTest
	0 Transporting,		0 WaitingForMRIStart,	0 WaitingForBloodTestStart,	0 WaitingForRadioStart
	0 Testing,		0 BloodTested,		0 MRITested,			0 RadioTested
	3 Released

KPIS :
Average time before 1st consultation (DTDT) : L1 DTDT = (day:0  hour:0  min:2)
Average time before 1st consultation (DTDT) : L2 DTDT = (day:0  hour:0  min:11)
Average time before 1st consultation (DTDT) : L3 DTDT = (day:0  hour:0  min:-1)
Average time before 1st consultation (DTDT) : L4 DTDT = (day:0  hour:0  min:-1)
Average time before 1st consultation (DTDT) : L5 DTDT = (day:0  hour:0  min:37)
Average length of stay : L1 LOS = (day:0  hour:1  min:30)
Average length of stay : L2 LOS = (day:0  hour:0  min:40)
Average length of stay : L3 LOS = (day:0  hour:0  min:-1)
Average length of stay : L4 LOS = (day:0  hour:0  min:-1)
Average length of stay : L5 LOS = (day:0  hour:1  min:16)


PATIENTS HISTORY : 
Patient2 Patient2 L2-->(arriving, day:0  hour:0  min:0), (registered, day:0  hour:0  min:0), (transporting, day:0  hour:0  min:4), (dropedInWaitingRoom, day:0  hour:0  min:6), (visited, day:0  hour:0  min:18), (released, day:0  hour:0  min:23), 
Patient5 Patient5 L5-->(arriving, day:0  hour:0  min:0), (registered, day:0  hour:0  min:0), (transporting, day:0  hour:0  min:8), (dropedInWaitingRoom, day:0  hour:0  min:10), (visited, day:0  hour:0  min:38), (released, day:0  hour:0  min:52), 
Patient3 Patient3 L2-->(arriving, day:0  hour:0  min:0), (registered, day:0  hour:0  min:0), (transporting, day:0  hour:0  min:2), (dropedInWaitingRoom, day:0  hour:0  min:4), (visited, day:0  hour:0  min:4), (waitingForRadio, day:0  hour:0  min:18), (transportation, day:0  hour:0  min:18), (waitingForRadioT, day:0  hour:0  min:22), (RadioTested, day:0  hour:0  min:22), (Test End, day:0  hour:0  min:39), (transportation, day:0  hour:0  min:52), (waitingForConsultation, day:0  hour:0  min:56), (released, day:0  hour:0  min:58), 
Patient4 Patient4 L5-->(arriving, day:0  hour:0  min:0), (registered, day:0  hour:0  min:0), (transporting, day:0  hour:0  min:10), (dropedInWaitingRoom, day:0  hour:0  min:12), (visited, day:0  hour:0  min:52), (released, day:0  hour:1  min:7), 
Patient1 Patient1 L1-->(arriving, day:0  hour:0  min:0), (registered, day:0  hour:0  min:0), (transporting, day:0  hour:0  min:0), (dropedInWaitingRoom, day:0  hour:0  min:2), (visited, day:0  hour:0  min:2), (waitingForMRI, day:0  hour:0  min:21), (transportation, day:0  hour:0  min:22), (waitingForMRIT, day:0  hour:0  min:26), (Start Test, day:0  hour:0  min:26), (Test End, day:0  hour:1  min:24), (transportation, day:0  hour:1  min:24), (waitingForConsultation, day:0  hour:1  min:28), (released, day:0  hour:1  min:30), 
Patient6 Patient6 L5-->(arriving, day:0  hour:0  min:0), (registered, day:0  hour:0  min:0), (transporting, day:0  hour:0  min:6), (dropedInWaitingRoom, day:0  hour:0  min:8), (visited, day:0  hour:0  min:21), (waitingForBloodTest, day:0  hour:0  min:38), (transportation, day:0  hour:0  min:38), (waitingForBloodTestT, day:0  hour:0  min:42), (bloodtested, day:0  hour:0  min:42), (Test End, day:0  hour:1  min:43), (transportation, day:0  hour:1  min:43), (waitingForConsultation, day:0  hour:1  min:47), (released, day:0  hour:1  min:49), 
