# Emergency Department Simulator

Imagine you are in charge of a whole Emergency Department, including a given amount of resources (Rooms of different types, Staff of different qualifications) and you want to know the maximum patients capacity it can absorb ? SimErgy is the solution !

Imagine you want to dimension you Emergency Department rationally, so that it could absorb a given flow of patients ?
SimErgy is the solution !

![SimErgy Principle](./images/principle.png)

- [Emergency Department Simulator](#emergency-department-simulator)
- [Run the simulation with SimErgy CLI](#run-the-simulation-with-simergy-cli)
  - [Run SimErgy](#run-simergy)
  - [List of command of the CLI](#list-of-command-of-the-cli)
  - [Set Up your simulation](#set-up-your-simulation)
  - [Run it as you want](#run-it-as-you-want)
  - [Save the results and investigate the perfect configuration](#save-the-results-and-investigate-the-perfect-configuration)
- [The Model simulated](#the-model-simulated)
  - [Patient Workflow](#patient-workflow)
  - [States of the Simulation Elements](#states-of-the-simulation-elements)

# Run the simulation with SimErgy CLI

## Run SimErgy
Go to the corresponding folder using the command prompt of your OS. You must then execute the command java -jar SimErgy.jar. Then appears the following display.

![Run SimErgy 1](./images/runSimergy1.png)

## List of command of the CLI
Then just type the help command to see what you can do through this CLI. 

![SimErgy Help](./images/runSimergy2.png)

## Set Up your simulation
You can then set your simulation using two modes:
- either you set it by hand using the different commands available in the previous list to add room, staff and facilities to your Emergency Department. For example adding a patient to the scheduled patient flow would be done with this command to add the patient Regis, of injury sevrity L2 who will arrive at time 15: ![Add Patient Exemple](./images/runSimergy3.png)
- or you set up your simulation by loading the content of a text file which contains the full description of your Hospital. We have included sample descriptions named 'UseCaseX.txt' which look like that: ![ED Simulation Description Example](./UseCase1.txt) ![Load ED from file](./images/runSimergy4.png)

## Run it as you want
Once it is all set up as you wish to simulate, you can run the simulation step by step with the command `runNextStep` or completely with the command `runSimulation <endTime>`.
You can also reset the simulation to its initial state with `resetAll`.

## Save the results and investigate the perfect configuration
At the end of the simulation, a summary displays the description of the simulation and its results showing the KPIs mesuring your Emergency Department's performance.
![Simulation Results](./images/runSimergy5.png)

The results of your simulation are saved at the project's root, in a txt file gathering the description of your ED, the simulation's final KPIs and the patients workflow.
[Here is a sample of simulation save](./SimErgy_Save_Hospital1_France.txt), and [her is another one](./SimErgy_Save_Hospital3_France.txt)


# The Model simulated

The SimErgy software is a simulation tool designed to meet the dimensioning needs of emergency hospital departments (ED: Emergency Department. It allows to simulate the treatment process of a patient flow from the following setting: 
- The patient flow : 
  - Typology of the patients: the patients do not all have the same degree of severity It is included in 'L1' (resuscitation)> 'L2' (emer-gency)> 'L3' (urgent)> 'L4' (less -urgent)> 'L5' (non-urgent) .
  -  Either deterministic: we record the patient typology and their arrival date before simulating the process of the emergency department.— Either probabilistic: a distribution of arrivals on a certain time interval is generated according to the selected law of probability. 
  -  Human resources available
     - Physicians who diagnose patients and recommend a specialized examination if necessary (Radio, Blood, MRI) .
     - Nurses q who can record the arrival of a patient, and bring them to the waiting room before consulting a Physician.
     - Transporters who transport patients between their consultation room and the specialized test rooms (Radio, MRI, Blood).
  - Mobile equipment: 
    - The Stretchers necessary for the transportation of serious patients ('L1', 'L2'). 
  - Infrastructures:
    - The WaitingRooms which are the waiting rooms before consultation.
    - The ShockRooms which are the consultation rooms reserved for patients of type 'L1', 'L2' .
    - The BoxRooms which are the other consultation rooms where all types of patients are accepted.
    - The RadioRooms which are the radiography test rooms.
    - The MRIRooms which are the test rooms IRM.
    - The BloodRooms which are the blood test rooms. 

From this initial configuration, SimErgy allows to simulate the treatment of the patient flow and to obtain in real time the perfor-man indicators the following:
- Door-to-doctor-time: average time between the arrival of patients and2
their 1st consultation.
- The Length-of-Stay: average time spent in the department (arrivals-> final verdict) One of the special features of our simulator is the return to the doctor after the test. Indeed, we made the choice of a final consultation of 2 minutes before the patient can leave the hospital. After performing a specialized test, the patient is transported back to his Physician (as soon as he is available) to obtain a verdict and to be released from the hospital, see [Patient Workflow](#patient-workflow).

## Patient Workflow

![Patient workflow inside Emergency Department](./images/patients_workflow.png)

The patient is the centerpiece of the simulations. It is he who undergoes most of the actions and whose state we want to know to know if the ED is performing well. The figure above shows a patient's journey from arrival to exit with the different states they can adopt. Each modification of his condition is done with a modification of his place in the ED database as you can see in detail right below. Also note that the physician treats the patient each time when he is in the state waitingForConsultation. However once it applies handleNewPatient and another time emitVerdict. 
Indeed, the physician is able to know what to do because he has the list of patients he has already seen. If he has never seen it then he must apply handleNewPatient, otherwise he must apply emitVerdict. Another special feature of our simulator is that the return to the doctor at the end of the test Room exam is done by transporter in 4 minutes and that we have allocated two minutes of duration at the final consultation before discharge from the hospital. We made this choice because there was nothing specific in the specifications on this subject. 
Figure above shows the patient's evolution within the ED. It shows all the interactions with its environment and the functions that make the patient evolve in ED. Concerning the treatment priority strategy, our simulator applies priority by degree of severity of the patients: if an event can be created with different patients it will always be created with the most serious patient.
This can sometimes lead to a phenomenon of "starvation" (in the algo-rithmic sense) temporary; non-emergency patients must wait until there are no more serious patients to be treated. Example: during arrivals, if 2 patients of level L1 and L5 arrive, the nurse will first transport the patient L1 to the waiting room for consultation.

## States of the Simulation Elements

![States of Elements](./images/states_map.png)
In order to have a management of the states of all the elements of each ED, we decided to equip the ED class with a database in the form of ArrayLists. They are all represented in diagram 7. Each box represents a list and each blue label represents a sublist with the dedicated state. Their names are identical to those in diagram 7. A special list is the edRegister list. It includes all the patients who have been registered in the hospital and this whatever their state. How an entity (room, patient, human resource...) Goes from one list to another when it changes state? This is managed by the methodessetState of each object. As you can see in the code, these methods take as argument a String with the state and are responsible for modifying the state and the place of the entity in the ED database. This avoids errors and manages these lists perfectly.

